# Tools for ExpansionHunter
Here are few tools/scripts for [ExpansionHunter](https://github.com/Illumina/ExpansionHunter).


## annotate_vcf.py
This script is for annotating ExpansionHunter's output VCF files with disease information found in the STRipy's database. Each locus that matches with the name in the database (first column @ https://stripy.org/database) will get annotated. For RFC1 locus id has to be either RFC1:AAGGG or RFC1:ACAGG.

Example use: `python3 annotate_vcf.py --input sample.vcf --output sample.ann.vcf`

NB! This can be also done on-the-fly by using curl and [STRipy's API](https://stripy.org/docs#annotatevcf). For example, `curl -F 'file=@sample.vcf' https://api.stripy.org/annotateVCF > sample.ann.vcf`


## bed_to_catalog.py

Convert a BED file to ExpansionHunter's variant catalogue (JSON) file. BED file has to contain four columns in the following order: chromosome, start, end and the motif.

Parameters:
--bed		input BED file
--out		output variant catalogue (JSON) file

Example use: `python3 bed_to_catalog.py --bed input_file.bed --out str_catalogue.json`

NB! This can be also done via a web form at [https://stripy.org/expansionhunter-catalog-creator](https://stripy.org/expansionhunter-catalog-creator)


## findLocus.py

This script is designed for processing locus.tsv files created by ExpansionHunter Denovo. It extracts all hits from the outlier or case-control locus.tsv  (`--tsv`) file where the p-value  (`--pval`) is below the specified threshold. It then identifies STR loci in the reference genome (`--reference`) using the approximate coordinates and generates a variant catalogue for ExpansionHunter. This catalogue includes all loci with the specified motif that is repeated at least 3 times in succession. The minimum number of repeats can be adjusted using the `--repeats` parameter (e.g., `--repeats 10`). The script can be used for either 'outlier' or 'case-control' types (`--type`).

```
python3 findLocus.py \
    --reference reference/hg38.fa \
    --tsv my_sample.outlier_locus.tsv \
    --type outlier \
    --pval 0.05 \
    --out my_sample.outlier_variant_catalogue.json
```


## extractSnpeffAnn.sh

If you possess a VCF file annotated with SnpEff, this script will output each locus along with all transcripts contained within it. The first argument should be the name of the VCF file, while the second and third optional parameters allow for specifying the minimum number of repeats and the genomic location (exon or intron) to be printed.

```
extractSnpeffAnn.sh my_sample.annotated.vcf 100 Exon > my_sample.annotated_100bp_or_longer_repeats_in_exons.tsv

```