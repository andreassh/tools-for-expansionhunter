import re
import os
import json
import argparse

###############################################################################
#                                                                             # 
# This simple script is for annotating ExpansionHunter's output VCF files     #
# with disease information found in STRipy's database (stripy.org/database)   #
# Example: python3 annotate_vcf.py --input sample.vcf --output sample.ann.vcf #
#                                                                             # 
###############################################################################


def annotateVCF(input_file, output_file, database_data):
	vcf_file = open(input_file, "r")
	vcf_file_content = vcf_file.read()

	catalog = []

	lines = re.split(r'\r\n|\n|\r', vcf_file_content)

	header = ""
	results_begin = False
	results = ""

	for line in lines:
		if line.startswith('#'):
			header += line + "\n"

			if line.startswith('##INFO=<ID=VARID'):
				header += '##INFO=<ID=DISID,Number=1,Type=String,Description="Disease ID as specified in the STRipy database">\n'
				header += '##INFO=<ID=DISNAME,Number=1,Type=String,Description="Disease name as specified in the STRipy database">\n'
				header += '##INFO=<ID=DISINHER,Number=1,Type=String,Description="Disease inheritance mode as specified in the STRipy database">\n'
				header += '##INFO=<ID=DISRANGE,Number=1,Type=String,Description="Range for both allele repeats for the disease as specified in the STRipy database">\n'

			if line.startswith('#CHROM'):
				results_begin = True
		else:
			if results_begin:
				lines_split = line.split("\t")

				if len(lines_split) > 1:
					locus = lines_split[7].split(";")[5].split("=")[1]
					repeats_arr = lines_split[9].split(":")[2].split("/")

					dis_addition = ";DISID=.;DISNAME=.;DISINHER=.;DISRANGE=."
					for record in database_data:
						if locus != record["Locus"]:
							continue

						locus_info = record

						dis_id = []
						dis_name = []
						dis_inheritance = []
						dis_rep_range_normal = []
						dis_rep_range_intermed = []
						dis_rep_range_path_cutoff = []

						for dis in locus_info["Diseases"]:
							dis_id.append(locus_info["Diseases"][dis]["DiseaseSymbol"])
							dis_name.append(locus_info["Diseases"][dis]["DiseaseName"].replace(",", "").replace("/", "-").replace(" ", "_").replace("'", ""))
							dis_inheritance.append(locus_info["Diseases"][dis]["Inheritance"])
							dis_rep_range_normal.append([locus_info["Diseases"][dis]["NormalRange"]["Min"], locus_info["Diseases"][dis]["NormalRange"]["Max"]])
							if (locus_info["Diseases"][dis]["IntermediateRange"] != "NA"):
								dis_rep_range_intermed.append([locus_info["Diseases"][dis]["IntermediateRange"]["Min"], locus_info["Diseases"][dis]["IntermediateRange"]["Max"]])
							dis_rep_range_path_cutoff.append(locus_info["Diseases"][dis]["PathogenicCutoff"])

						range_dict = {}
						match = {}
						range_diseases = {}

						for dis_no, disease in enumerate(dis_id):
							for allele, rep in enumerate(repeats_arr):
								rep = int(rep)
								match.setdefault(disease, {})[rep] = 0
								range_dict.setdefault(disease, {})[rep] = ""

								if dis_rep_range_normal[dis_no][0] <= rep <= dis_rep_range_normal[dis_no][1]:
									match[disease][rep] = 1
									range_dict[disease][rep] = "Normal"

								if dis_rep_range_intermed:
									if dis_rep_range_intermed[dis_no][0] <= rep <= dis_rep_range_intermed[dis_no][1]:
										match[disease][rep] = 1
										range_dict[disease][rep] = "Intermediate"

								if rep >= int(dis_rep_range_path_cutoff[dis_no].split("-")[0]):
									match[disease][rep] = 1
									range_dict[disease][rep] = "Pathogenic"

								if match[disease][rep] == 0:
									range_dict[disease][rep] = "Unknown"
							range_diseases[disease] = "|".join(range_dict[disease].values())

							if len(range_dict[disease].values()) == 1:
								range_diseases[disease] = range_diseases[disease]+"|"+range_diseases[disease]

						range_str = ",".join(range_diseases.values())


						dis_id_joined = ",".join(dis_id)
						dis_name_joined = ",".join(dis_name)
						dis_inheritance_joined = ",".join(dis_inheritance)
						dis_addition = f";DISID={dis_id_joined};DISNAME={dis_name_joined};DISINHER={dis_inheritance_joined};DISRANGE={range_str}"

					orig_info_field = lines_split[7]
					new_line = line.replace(orig_info_field, orig_info_field + dis_addition)
					results += new_line + "\n"

	f = open(output_file, "w")
	f.write(header + results)
	f.close()

	return True


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--input",	required = True, help = "Input ExpansionHunter's VCF file)")
	parser.add_argument("--output",	required = True, help = "Output file name of annotated VCF file")
	args = parser.parse_args()

	scriptpath = os.path.dirname(os.path.abspath(__file__))
	cat_file = os.path.join(scriptpath, 'catalog.json')
	err = False

	if not os.access(args.input, os.R_OK):
		err = True
		print("ERROR: Input VCF file could not be found")
	if not os.access(cat_file, os.R_OK):
		err = True
		print("ERROR: STRipy's catalogue file could not be found")

	if not err:
		with open(cat_file, mode='r') as cat_file:
			database_data = json.load(cat_file)
			annotateVCF(args.input, args.output, database_data)
