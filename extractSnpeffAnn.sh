#!/bin/bash

# Usage: extractSnpeffAnn.sh filename.vcf [minimum repeat length in bp] [region name - Exon or Intron]

awk -v bpThreshold="$2" -v regionType="$3" -F'\t' '{
  if ($1 ~ /^#/) next;  # Skip header lines
  varid = motif = distance = "";  # Initialize variables
  if (regionType != "Exon" && regionType != "Intron") regionType = "Intron|Exon";  # Default to both if unspecified or invalid

  # Look for VARID and RU in the 8th field (INFO field)
  if ($8 ~ /VARID=/) {
    match($8, /VARID=[^;]+/);
    varid = substr($8, RSTART+6, RLENGTH-6);  # Extract VARID value
    sub(/-/, ":", varid);  # Replace the first "-" with ":"
  }
  if ($8 ~ /RU=/) {
    match($8, /RU=[^;]+/);
    motif = substr($8, RSTART+3, RLENGTH-3);  # Extract RU (motif) value
    motifLength = length(motif);  # Calculate the length of the motif
  }
  # Attempt to extract CLOSEST value (before the first "|" if present)
  if ($8 ~ /CLOSEST=/) {
    match($8, /CLOSEST=[^;|]+/);
    distance = substr($8, RSTART+8, RLENGTH-8) " bp";  # Extract CLOSEST value and append " bp"
  }

  split($NF, genotypeInfo, ":");
  genotype = genotypeInfo[3];
  split(genotype, gtParts, "/");
  genotypeCI = genotypeInfo[4];

  # Conditionally print even when no CLOSEST tag is found
  if (distance == "" && length(gtParts) > 1) {
    print gtParts[2] " " "Locus: " varid "\tMotif: " motif "\tGenotype: " genotype;
  } else {
    # Check if either allele in the genotype meets/exceeds the threshold
    allele1 = gtParts[1] + 0;  # Convert to numeric for comparison
    allele2 = gtParts[2] + 0;  # Convert to numeric for comparison

    if (allele1 * motifLength >= bpThreshold || allele2 * motifLength >= bpThreshold) {
      for (i = 5; i <= NF-1; i++) {
        split($i, annotations, "|");

        for (j in annotations) {
          transcriptId = geneName = "";
          if (annotations[j] ~ regionType) {  # Filter based on region type
            split(annotations[j], parts, ",");
            for (k in parts) {
              if (parts[k] ~ /Transcript:/) {
                split(parts[k], transcriptPart, ":");
                transcriptId = transcriptPart[2];
                sub(/ .*/, "", transcriptId);
              } else if (parts[k] ~ /Gene:/) {
                split(parts[k], genePart, ":");
                geneName = genePart[2];
                sub(/ .*/, "", geneName);
              }
            }
            split(parts[1], regionDetails, ":");
            regionType = regionDetails[1];
            regionNo = regionDetails[2];
            regionNoTot = regionDetails[3];
            regionInfo = (regionNo != "" && regionNoTot != "" ? " (" regionNo "/" regionNoTot ")" : "");

            if (transcriptId && geneName && length(gtParts) > 1) {
              print gtParts[2] " " "Locus: " varid "\tMotif: " motif "\tGenotype: " genotype "\tCI: " genotypeCI "\tDistance: " distance "\tRegion: " regionType regionInfo " of transcript " transcriptId " in gene " geneName;
            }
          }
        }
      }
    }
  }
}' "$1" | sort -n -k1,1 -r | cut -d' ' -f2-
