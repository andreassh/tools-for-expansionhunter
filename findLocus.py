import argparse
import regex
import pysam
import json
import math
import sys
import re

def zscore_to_pval(z_score):
    """
    Convert a z-score to a p-value - two sided test
    """
    return 1.0 - 0.5 * (1 + math.erf(z_score / math.sqrt(2)))

def getReferenceFastaSequence(fastafile, region, flanking_size=0):
    """
    Get reference fasta sequence for given region.
    """
    chrom, start, end = regex.split(':|-', region)
    start = int(start) - flanking_size
    end = int(end) + flanking_size

    fasta_open = pysam.Fastafile(fastafile)
    seq_fasta = fasta_open.fetch(chrom, start, end).upper()

    return seq_fasta

def find_repeated_motifs(sequence, motif, min_repeats = 3):
    """
    Find all regions in the sequence where the motif is repeated at least min_repeats times.
    Returns a list of tuples with the start position and length of each region.
    """
    regions = []
    motif_length = len(motif)
    last_end = 0  # Track the end of the last match

    # Create a regex pattern for the repeated motif
    pattern = f"(?=({motif})\\1{{{min_repeats - 1},}})"
    for match in re.finditer(pattern, sequence):
        start = match.start()
        if start < last_end: # Skip if this match overlaps with the previous one
            continue

        end = match.end()
        # Adjust the end position to include all repeats of the motif
        while sequence[end:end + motif_length] == motif:
            end += motif_length
        last_end = end  # Update the last_end to the current end

        region_length = end - start
        regions.append((start, region_length))

        repeated_sequence = sequence[start:end]
    
    return regions

def read_data_file(filename, analysis_type, min_score):
    data = []

    with open(filename, 'r') as file:
        next(file)  # Skip the header line
        for line in file:
            columns = line.strip().split('\t')
            if len(columns) >= 6:
                contig, start, end, motif, score = columns[:5]
                score = float(score)

                if analysis_type == "case-control":
                    samples = columns[6]
                elif analysis_type == "outlier":
                    samples = columns[5]
                    score = zscore_to_pval(score)

                if score < min_score:
                    region = f"{contig}:{start}-{end}"
                    sample_ids = [sample.split(':')[0] for sample in samples.split(',')]

                    data.append({'region': region, 'motif': motif, 'sample_ids': sample_ids})

    return data

def main():
    parser = argparse.ArgumentParser(description="Find STRs in DNA sequences.")
    parser.add_argument('--reference', required=True, help='Path to the reference fasta file.')
    parser.add_argument('--tsv', required=True, help='Path to the TSV file with locus information.')
    parser.add_argument('--type', choices=['case-control', 'outlier'], required=True, help='Type of analysis: case-control or outlier.')
    parser.add_argument('--pval', type=float, required=True, help='P-value required for significant loci.')
    parser.add_argument('--repeats', type=int, required=False, default=3, help='Minimum length of repeats required in the reference genome to define an STR locus (default: 3).')
    parser.add_argument('--out', required=True, help='Output file name for variant catalogue (JSON)')

    args = parser.parse_args()

    ref_file = args.reference
    locus_file = args.tsv
    analysis_type = args.type
    min_score = args.pval
    min_repeats = args.repeats
    out_file = args.out

    regions_and_motifs = read_data_file(locus_file, analysis_type, min_score)

    loci_data = []

    for item in regions_and_motifs:
        ref_region = item['region']
        motif = item['motif']
        sample_ids = item['sample_ids']

        sequence = getReferenceFastaSequence(ref_file, ref_region)
        ref_chrom, ref_start, ref_end = regex.split(':|-', ref_region)

        if int(ref_end)-int(ref_start) < len(motif):
            print(f"{ref_region} - region is too short to find repeats of motif '{motif}'")
            continue

        regions = find_repeated_motifs(sequence, motif, min_repeats)

        if regions:
            print(f"{ref_region} - found {len(regions)} {'locus' if len(regions) == 1 else 'loci'} with repeated motif '{motif}' in {ref_region} reference region, the following samples with expanded alleles were identified: {', '.join(sample_ids)}")

            for start, length in regions:
                locus_start = int(ref_start) + int(start)
                locus_end = locus_start + int(length)
                locus_name = f"{ref_chrom}-{locus_start}-{locus_end}"
                locus_region = f"{ref_chrom}:{locus_start}-{locus_end}"
                
                print(f" ↳ {locus_region} ({int(length)/len(motif)} {motif} repeats)")

                locus_dict = {
                    "LocusId": locus_name,
                    "LocusStructure": f"({motif})*",
                    "ReferenceRegion": locus_region,
                    "VariantType": "Repeat"
                }
                loci_data.append(locus_dict)
        else:
            print(f"{ref_region} - no regions with repeated motif '{motif}' were found")

    # Write the list of dictionaries to a JSON file
    if loci_data:
        with open(out_file, 'w') as file:
            json.dump(loci_data, file, indent = 4)
            print(f"\n------------------------------Finished------------------------------")
            print(f"Variant catalog created ({out_file}) containing {len(loci_data)} {'locus' if len(loci_data) == 1 else 'loci'}.")
    else:
        print(f"\n------------------------------------------Finished------------------------------------------")
        print("Variant catalogue was not created as no loci were found from files and/or match the criteria.")

if __name__ == "__main__":
    main()
